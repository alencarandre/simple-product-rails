require 'rails_helper'

RSpec.describe Product, type: :model do
  describe '#validations' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:product)).to be_valid
    end

    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(100) }

    it { should validate_presence_of(:description) }
    it { should validate_length_of(:description).is_at_most(512) }

    it { should validate_presence_of(:product_variations) }
  end

  describe '#associations' do
    it { is_expected.to have_many(:product_variations) }
    it { should accept_nested_attributes_for :product_variations }
  end
end
