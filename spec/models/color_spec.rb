require 'rails_helper'

RSpec.describe Color, type: :model do
  it 'has a valid factory' do
    expect(FactoryBot.build(:color)).to be_valid
  end

  it { should validate_presence_of(:name) }
end
