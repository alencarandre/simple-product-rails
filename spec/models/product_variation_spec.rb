require 'rails_helper'

RSpec.describe ProductVariation, type: :model do
  describe '#validations' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:product_variation)).to be_valid
    end

    it { should validate_presence_of(:quantity) }
  end

  describe '#associations' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:color) }
    it { is_expected.to belong_to(:size) }
  end
end
