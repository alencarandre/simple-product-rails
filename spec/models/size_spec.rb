require 'rails_helper'

RSpec.describe Size, type: :model do
  it 'has a valid factory' do
    expect(FactoryBot.build(:size)).to be_valid
  end

  it { should validate_presence_of(:name) }
end
