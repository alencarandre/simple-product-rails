require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  describe '#create' do
    it 'creates product if data is valid' do
      color = FactoryBot.create(:color)
      size1 = FactoryBot.create(:size)
      size2 = FactoryBot.create(:size)
      product_data = {
        name: 'Product name',
        description: 'Product description',
        product_variations_attributes: [{
          color_id: color.id,
          size_id: size1.id,
          quantity: 1
        }, {
          color_id: color.id,
          size_id: size2.id,
          quantity: 10
        }]
      }

      expect {
        post :create, product: product_data
      }.to change { Product.count  }
      .from(0)
      .to(1)

      product = Product.first
      variations = product.product_variations.to_a

      expect(product.name).to eq('Product name')
      expect(product.description).to eq('Product description')
      expect(variations.count).to eq 2
      expect(variations.first.color).to eq(color)
      expect(variations.first.size).to eq(size1)
      expect(variations.second.color).to eq(color)
      expect(variations.second.size).to eq(size2)
    end

    it 'does not create product if data is invalid' do
      color = FactoryBot.create(:color)
      size = FactoryBot.create(:size)
      product_data = {
        product_variations_attributes: [{
          color_id: color.id,
          size_id: size.id,
          quantity: 1,
        }]
      }

      expect {
        post :create, product: product_data
      }.to_not change { Product.count  }
    end
  end

  describe '#update' do
    it 'updates product if data is valid' do
      color = FactoryBot.create(:color)
      size = FactoryBot.create(:size)
      product = FactoryBot.create(:product)
      old_variation = product.product_variations.first
      product_data = {
        name: 'Product updated',
        description: 'New product description',
        product_variations_attributes: [{
          id: old_variation.id,
          _destroy: true
        }, {
          color_id: color.id,
          size_id: size.id,
          quantity: 1
        }]
      }

      put :update, id: product.id, product: product_data
      variations = product.reload.product_variations

      expect(product.name).to eq('Product updated')
      expect(product.description).to eq('New product description')
      expect(variations.count).to eq(1)
      expect(variations).to_not include(old_variation)
      expect(variations.first.color).to eq(color)
      expect(variations.first.size).to eq(size)
    end

    it 'does not update product if data is invalid' do
      product = FactoryBot.create(:product)
      old_name = product.name
      old_description = product.description
      product_data = {
        name: nil,
        description: 'New product description'
      }

      put :update, id: product.id, product: product_data
      product.reload

      expect(product.name).to eq(old_name)
      expect(product.description).to eq(old_description)
    end
  end

  describe '#destroy' do
    it 'destroys product' do
      product1 = FactoryBot.create(:product)
      product2 = FactoryBot.create(:product)

      expect {
        delete :destroy, id: product1.id
      }.to change { Product.count }
      .from(2)
      .to(1)

      expect(Product.all).to match_array [product2]
    end
  end
end
