FactoryBot.define do
  factory :color do
    name { Faker::Color.name }
  end
end
