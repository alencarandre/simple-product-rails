FactoryBot.define do
  factory :product_variation do
    color
    size
    quantity { 10 }

    after(:build) do |variation|
      variation.product ||= FactoryBot.build(:product, product_variations: [variation])
    end
  end
end
