FactoryBot.define do
  factory :size do
    name { %w(PP P M G GG XG).sample }
  end
end
