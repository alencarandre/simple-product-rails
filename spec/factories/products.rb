FactoryBot.define do
  factory :product do
    name { Faker::Lorem.sentence(1) }
    description { Faker::Lorem.paragraphs(1).join }

    after(:build) do |product|
      if product.product_variations.blank?
        product.product_variations << FactoryBot.build(:product_variation, product: product)
      end
    end
  end
end
