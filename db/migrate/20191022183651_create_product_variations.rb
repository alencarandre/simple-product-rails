class CreateProductVariations < ActiveRecord::Migration
  def change
    create_table :product_variations do |t|
      t.references :product, index: true
      t.references :color, index: true
      t.references :size, index: true
      t.integer :quantity

      t.timestamps null: false
    end

    add_foreign_key :product_variations, :products
    add_foreign_key :product_variations, :colors
    add_foreign_key :product_variations, :sizes
  end
end
