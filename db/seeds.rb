# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

colors =  %w(Amarelo Azul Cinza Laranja Preto Rosa Roxo Verde Vermelho Vinho)
colors.each do |color|
  Color.where(name: color).first_or_create!
end

sizes = %w(PP P M G GG XG)
sizes.each do |size|
  Size.where(name: size).first_or_create!
end

product_names = [
  'Camiseta Pullman',
  'Camiseta Panco',
  'Camiseta Seven boys',
  'Calça Caqui',
  'Calça Cenoura',
  'Calça Batata',
]

product_names.each do |product_name|
  variations = []
  4.times do
    variations << {
      color_id: Color.all.sample.id,
      size_id: Size.all.sample.id,
      quantity: 10
    }
  end

  Product
    .where(name: product_name)
    .first_or_create!(
      name: product_name,
      description: Faker::Lorem.paragraphs(1).join,
      product_variations_attributes: variations
    )
end
