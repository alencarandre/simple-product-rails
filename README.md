# Simple Product Rails

## Como executar essa aplicação

### Dependencias
- Docker
- Docker Compose

### Antes de tudo, rodar o setup

```bash
scripts/setup
```

### Para rodar os testes

```bash
scripts/test
```

### Para rodar o web server

```bash
scripts/web
```

### Acessar a aplicação

Acesse o link http://localhost:3000

## Gems adicionais utilizadas

`cocoon` Manipular mais facil o nested form no front end.
`responders` Pra tratar o retorno na controller de acordo com o resultado da operação do ActiveRecord
`shoulda-matchers` Adiciona alguns matchers para usar no rspec
`factory_bot` Subistituto do Factory Girl
`rails-assets-bootstrap` Para utilizar o bootstrap nas views
`faker` Gerar dados fake para seed e rspec
