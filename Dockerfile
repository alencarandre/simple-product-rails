FROM ruby:2.2.0

ADD . /opt/simple-product-rails
WORKDIR /opt/simple-product-rails

RUN apt-get update && apt-get install -y \
  nodejs \
  postgresql-client

RUN bundle install

EXPOSE 3000
