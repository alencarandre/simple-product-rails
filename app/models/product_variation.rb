class ProductVariation < ActiveRecord::Base
  belongs_to :product
  belongs_to :color
  belongs_to :size

  validates :quantity, presence: true
end
