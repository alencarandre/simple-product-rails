class Product < ActiveRecord::Base
  validates :name, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 512 }

  has_many :product_variations, dependent: :destroy

  accepts_nested_attributes_for :product_variations,
    allow_destroy: true,
    reject_if: :all_blank

  validates :product_variations, presence: true
end
