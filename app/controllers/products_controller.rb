class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    @product.product_variations.build
  end

  def show
    @product = find_product(params[:id])
  end

  def create
    @product = Product.create(product_params)

    respond_with @product
  end

  def edit
    @product = find_product(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    @product.update_attributes(product_params)

    respond_with @product
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_with @product
  end

  private

  def find_product(id)
    Product
      .joins(product_variations: [:color, :size])
      .includes(product_variations: [:color, :size])
      .find(id)
  end

  def product_params
    params
      .require(:product)
      .permit(
        :name,
        :description,
        product_variations_attributes: [:id, :color_id, :size_id, :quantity, :_destroy]
      )
  end
end
