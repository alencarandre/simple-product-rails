module PagesHelper
  def page_header(title)
    content_tag :div, class: 'page-header' do
      content_tag :h1 do
        concat title
        concat align_right { yield } if block_given?
      end
    end
  end

  def button_new(path)
    link_to path, class: 'btn btn-primary' do
      concat content_tag :i, ' ', class: 'glyphicon glyphicon-plus'
      concat I18n.t('buttons.new')
    end
  end

  def button_edit(path)
    link_to path, class: 'btn btn-default' do
      concat content_tag :i, ' ', class: 'glyphicon glyphicon-edit'
      concat I18n.t('buttons.edit')
    end
  end

  def button_back(path)
    link_to path, class: 'btn btn-default' do
      concat content_tag :i, ' ', class: 'glyphicon glyphicon-chevron-left'
      concat I18n.t('buttons.back')
    end
  end

  def button_destroy(path)
    link_to path, class: 'btn btn-danger', method: :delete, data: { confirm: I18n.t('messages.are_you_sure') } do
      concat content_tag :i, ' ', class: 'glyphicon glyphicon-trash'
      concat I18n.t('buttons.destroy')
    end
  end

  def align_right
    content_tag :div, class: 'pull-right' do
      yield
    end
  end
end
