module MainMenuHelper

  def main_menu
    content_tag :ul, class: 'nav navbar-nav' do
      resources.map do |resource|
        concat build_item(resource)
      end
    end
  end

  private

  def build_item(resource)
    content_tag :li, class: active_class(resource.first) do
      link_to I18n.t("main_menu.#{resource.first}"), resource.last
    end
  end

  def resources
    {
      products: products_path
    }
  end

  def active_class(resource_name)
    'active' if controller_name == resource_name.to_s
  end
end
